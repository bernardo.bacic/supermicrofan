import setuptools

# run this as: python3  setup.py  bdist_wheel
#              check dist directory
#          +   pip install <wheel>

#with open( 'README.org', 'r') as fh:
#    long_description = fh.read()


setuptools.setup( name='supermicro-fan-control',
                  version='0.0.1',
                  scripts=['fan_control.py',] ,
                  author='TriPolarBear',
                  author_email="gxs5oft5@duck.com",
                  description="Upload files to a remote FTP host",
                  long_description='see README.org',
                  long_description_content_type="text/markdown",
                  url="https://gitlab.com/bernardo.bacic/supermicrofan.git",
                  packages=setuptools.find_packages(),
                  data_files=[ ('opt/test/', ['data/setup.py']),
                              ],
                  classifiers=[ "Programming Language :: Python :: 3",
                                "License :: OSI Approved :: GPL 3",
                                "Operating System :: POSIX :: Linux",
                                "Topic :: System :: Hardware",
                                "Development Status :: 2 - Pre-Alpha"
                               ],
                 )
