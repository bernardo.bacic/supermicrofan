#!/usr/bin/env python3
"""A simple script to plot an Alveo temperature/power consumption graph from
entries in system log created by fan_control.py application. See comments at
the bottom how to extract and process data from the system log.
"""

import os
import matplotlib.pyplot as plt
#import matplotlib.font_manager as font_mgr
import numpy as np

FONT_SZ = 20

def plot_temp( data_file ):
    '''Create a temperature graph out of passed data.'''
    if not os.path.exists( data_file ):
        print( '\nWARNING no data in %s file yet\n' % data_file )
        return False

#    plt.ion() # FIXME this BLOCKS (!?)
    fig = plt.figure(figsize=(15, 12))
    plt.title( 'psi-perentie2 FPGA temperature based fan control', fontsize=FONT_SZ )
    # NEED to turn off double ticks
    plt.tick_params(labelcolor='none', top=False, bottom=False, left=False, right=False)

    ax1 = fig.add_subplot( 111 )
    #                      |||
    #                      ||n-th plot
    #                      |# of columns
    #                      # of rows

    # temperature plot
    y = np.loadtxt( data_file, usecols=1 )
    legend1 = ax1.plot( y, label='temp', color='green' )

    ax1.grid( True )
    ax1.set_xlabel( 'time', fontsize=FONT_SZ)
    ax1.set_ylabel( 'Temperature [°C]', fontsize=FONT_SZ)
    ax1.set_ylim( 40, 60 )
    ax1.text( 100.5, 50.2, 'slow\nfan', fontsize=FONT_SZ )
    ax1.text( 126, 50.2, 'fast\nfan', fontsize=FONT_SZ )

    # power plot
    ax2 = ax1.twinx()
    y = np.loadtxt( data_file, usecols=2 )
    legend2 = ax2.plot( y, label='power', color='red' )
    ax2.set_ylim( 32, 38 )
    ax2.set_ylabel( 'Power [W]', fontsize=FONT_SZ)

    # combine legends
    legends = legend1 + legend2
    labels  = [_.get_label() for _ in legends]
    ax1.legend( legends, labels, loc='lower right', fontsize=FONT_SZ)

    plt.savefig( 'resources/temp_fan.png' )
#    print( 'interactive', plt.isinteractive() )
    plt.show()
    # C-w to quit the graph


# extract fan_control data from overall journalcfg data:
#   journalctl -u supermicro-fan-control --no-pager -S "14:30:00" |grep ': 53:' > ~/temp.log
# (grep is needed to filter out lines indicating fan speed change)
#
# use awk to extract e.g. 1st PCIe Alveo data from the raw journal:
#   awk '{printf "%s:  %s %s\n", $6, $7, $9}' temp.log > temp_1.txt
plot_temp( 'temp_1.txt' )
