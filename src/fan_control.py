#!/usr/bin/env python3
#  -*- coding: utf-8 -*
"""Control Supermicro server fans depending on the Alveo card FPGA
temperature. Normally runs as a systemd daemon.
"""

from configparser import ConfigParser
import enum
import json
import os
import re
import requests
import signal
import subprocess
import sys
import time
import pyxrt

# required external utilities
IPMI_CMD = '/usr/local/bin/ipmicfg' # NOTE: user needs 'sudo' rights to run this
LSPCI    = '/usr/bin/lspci'
CONFIG_FNAME = 'fan.conf' # configuration file name; same directory as this script
PID_FILE = '/tmp/fanctl.pid'
NOTIFICATION_URL = "https://ntfy.sh/AlveoServerEvent-syd" # external notifications

class SpinUpHigh ( enum.IntEnum ):
    """The FPGA temperature values above which we'll spin the chasis fans hard.
    Defines the default value and acceptable range.
    """
    min_val = 45 # don't accept a value lower than this
    max_val = 85 # don't accept a value higher than this
    default = 52 # the default value; can be overriden from config file
                 # if the value is in the [min_val, max_val] range

class SpinUpMed ( enum.IntEnum ):
    """The FPGA temperature values above which we'll spin the chasis fans at
    moderate speed (SupermicroFan.MID).
    Defines the default value and acceptable range.
    """
    min_val = 45 # don't accept a value lower than this
    max_val = 70 # don't accept a value higher than this
    default = 48 # the default value; can be overriden from config file
                 # if the value is in the [min_val, max_val] range


class SpinDown ( enum.IntEnum ):
    """Minimal, maximal, default FPGA temperature values at which we'll spin
    the chasis fans at lower speed.
    """
    min_val = 30 # don't accept a value lower than this
    max_val = 55 # don't accept a value higher than this
    default = 48 # the default value; can be overriden from config file
                 # if the value is in the [min_val, max_val] range


class CheckPeriod ( enum.IntEnum ):
    """The period (in seconds) between two consecutive temperature checks.
    Defines the default value and acceptable range.
    """
    default = 10 # the default value; can be overriden from config file
                 # if the value is in the [min_val, max_val] range
    min_val = 3  # don't accept a value lower than this
    max_val = 60 # don't accept a value higher than this


# global storage for configuration values
START_BLOWING_HIGH_TEMP = SpinUpHigh.default
START_BLOWING_MID_TEMP = SpinUpMed.default
STOP_BLOWING_HARD_TEMP  = SpinDown.default
TEMPERATURE_CHECK_PERIOD_SEC = CheckPeriod.default


class SupermicroFan:
    SPEED_HIGH = 1
    SPEED_LOW  = 2
    SPEED_MID  = 4
    """Numerical values used by ipmicfg utility when SETTING the fan speed"""

    SPEED_STR_TO_INT = {
        'Optimal':        SPEED_LOW,
        'Full':           SPEED_HIGH,
        'Heavy IO speed': SPEED_MID
    }
    """Map speed mode string (as returned by ipmicfg utility) to a corresponding
    numerical value - used by the same utility when SETTING the fan speed"""

    def __init__( self ):
        'constructor ...'
        self.speed_int, _ = self.get_speed()
        self._high_speed_on = False
        self._high_speed_start_time = 0
        self._last_notification_time = 0
        self.HIGH_SPEED_DURATION_SEC = 900  # notification sent if fans spin fast for this long
                                            # meaning they are ineffective
        self.NOTIFICATION_PERIOD_SEC = 7200

    def get_speed( self ) -> tuple:
        '''Get the current fan speed. Return a tuple of fan speed code (SPEED_HIGH etc)
        and description as returned by ipmicfg utility'''
        speed_str = self._get_speed()
        self.speed_int = SupermicroFan.SPEED_STR_TO_INT[ speed_str] if len( speed_str ) > 2 else -1
        return (self.speed_int, speed_str)

    def set_speed( self, speed: int ) -> None:
        if speed not in (self.SPEED_LOW, self.SPEED_MID, self.SPEED_HIGH):
            print( f'ERROR: invalid speed setting {speed}' )
            return
        if speed in (self.SPEED_HIGH,): # self.SPEED_MID): #=> for testing
            self._high_speed_on = True
            self._high_speed_start_time = time.time()
        else:
            self._high_speed_on = False
        self._last_notification_time = 0
        subprocess.call( [ 'sudo', IPMI_CMD, '-fan', str(speed) ] )
        # the only output from the above command is 'Done.' - not very useful

    def adjust_speed( self, curr_temperature ):
        'Set the fan speed proportional to the current Alveo temperature.'
        fan_speed_changed = True   # assume we're about to change the fan speed
        fan_spins_fast = self.speed_int == self.SPEED_HIGH
        if curr_temperature >= START_BLOWING_HIGH_TEMP and not fan_spins_fast:
            self.set_speed( SupermicroFan.SPEED_HIGH )
        elif curr_temperature <= STOP_BLOWING_HARD_TEMP and fan_spins_fast:
            self.set_speed( SupermicroFan.SPEED_LOW )
        elif curr_temperature >= START_BLOWING_MID_TEMP and self.speed_int == self.SPEED_LOW:
            self.set_speed( SupermicroFan.SPEED_MID )
        else:
            fan_speed_changed = False

        # indicate what's changed, if anything
        if fan_speed_changed:
            self.speed_int, fan_mode_str = self.get_speed()
            print( f'Fan speed set to {fan_mode_str}' )

        time_difference = int( time.time() - self._high_speed_start_time )
        if self._high_speed_on and time_difference >= self.HIGH_SPEED_DURATION_SEC:
            self._send_notification( f'Fans running at high speed for {time_difference} sec' )

    def _get_speed( self ) -> str:
        console_out = subprocess.check_output( [ 'sudo', IPMI_CMD, '-fan'] )
        for line in console_out.decode().split(sep='\n'):
            m = re.match( 'Current Fan Speed Mode is \[ (.+) Mode \]', line )
            if m:
                return m.group( 1 )
        return ""

    def _send_notification( self, msg ):
        '''Send a notification to an external monitoring service, but not
        too often'''
        now = time.time()
        if self._last_notification_time == 0 or \
           (now - self._last_notification_time) > self.NOTIFICATION_PERIOD_SEC:
            requests.post( NOTIFICATION_URL, data=f"{msg}".encode(encoding='utf-8') )
            self._last_notification_time = now
            print( 'Sent external notification' )


def get_alveo_bdfs() -> list[str]:
    'Get a list PCIe slots (BDFs) populated by Alveo cards'
    bdf = []
    console_out = subprocess.check_output( [ LSPCI ] )
    for line in console_out.decode().split(sep='\n'):
        m = re.match( '(.{2}:00\.1).+Xilinx', line )
        if m:
            bdf.append( '0000:' + m.group( 1 ) )
    return bdf


# FIXME return type
def get_alveo_sensor_readings( devices: list[str] ):
    '''Using Xilinx XRT library, read temperature from Alveo cards plugged
    into PCIe slots listed in 'devices' argument.
    '''
    temps = []
    power = []
    POWER_KEY = "power_consumption_watts"
    for bdf in devices:
        dev = pyxrt.device( bdf )
        jsn_str = dev.get_info( pyxrt.xrt_info_device.electrical )
        jsn = json.loads( jsn_str )
        power.append( float( jsn[ POWER_KEY ] ) if POWER_KEY in jsn else 0 )

        jsn_str = dev.get_info( pyxrt.xrt_info_device.thermal )
        jsn = json.loads( jsn_str )
        if 'thermals' in jsn:
            for item in jsn['thermals']:
                if item[ 'description' ] == 'FPGA':
                    temps.append( int(item[ "temp_C" ]) )
                    # bail out of inner loop, we found temperature for this device
                    break
    return temps, power


def signal_handler_usr1( signum, stack_frame ):
    'Reload the config file on USR1 signal.'
    signame = signal.Signals(signum).name
    print( f'Signal handler got {signame} ({signum})' )
    load_config()

def signal_handler_term( signum, stack_frame ):
    'Remove PID file on TERM signal.'
    signame = signal.Signals(signum).name
    print( f'Signal handler got {signame} ({signum})' )
    os.remove( PID_FILE )
    sys.exit()


def load_config():
    """Override the defaults with config file values.
    Check the configuration file values are sensible; if not don't change
    the existing value.
    """
    global START_BLOWING_HIGH_TEMP
    global STOP_BLOWING_HARD_TEMP
    global TEMPERATURE_CHECK_PERIOD_SEC

    cfg = ConfigParser()
    if len( cfg.read( CONFIG_FNAME ) ) > 0:
        try:
            # the highest fan speed setting
            spin_up_temp = int( cfg['THRESHOLD'].get( 'spin_up_fast',  SpinUpHigh.default ) )
            # check the valid range of temperature at which we should spin up fans
            if SpinUpHigh.min_val <= spin_up_temp <= SpinUpHigh.max_val:
                START_BLOWING_HIGH_TEMP = spin_up_temp

            # the medium fan speed setting
            spin_up_temp = int( cfg['THRESHOLD'].get( 'spin_up_med',  SpinUpMed.default ) )
            # check the valid range of temperature at which we should spin up fans
            if SpinUpMed.min_val <= spin_up_temp <= SpinUpMed.max_val:
                START_BLOWING_MID_TEMP = spin_up_temp

            # check the valid range of temperature at which we should spin down fans
            spin_down_temp = int( cfg['THRESHOLD'].get( 'spin_down', SpinDown.default ) )
            if SpinDown.min_val <= spin_down_temp <= SpinDown.max_val:
                STOP_BLOWING_HARD_TEMP = spin_down_temp
            period_val = int( cfg['TEMPERATURE_CHECK'].get('period_seconds', CheckPeriod.default ) )
            if CheckPeriod.min_val <= period_val <= CheckPeriod.max_val:
                TEMPERATURE_CHECK_PERIOD_SEC = period_val
        # handle garbage in configuration file:
        except Exception as e:
            print( f'{CONFIG_FNAME} value caused exception {e}' )
    print( f'HIGH fan speed when above: {START_BLOWING_HIGH_TEMP}, MEDIUM fan speed when above {START_BLOWING_MID_TEMP}, SLOW fan when at/below: {STOP_BLOWING_HARD_TEMP}, check every: {TEMPERATURE_CHECK_PERIOD_SEC} sec')


def announce_pid():
    'Let the operator know our PID to which they can send USR1 signal.'
    pid = os.getpid()
    print( 'My PID', pid )
    with open( PID_FILE, 'w' ) as outfile:
        outfile.write( str(pid) )


def log( pcie_slots: list[str], temps: list[int], power: list[float] ):
    'Show FPGA temperature and power consumption for each Alveo'
    for i, pci in enumerate( pcie_slots ):
        print( f'{pci[5:7]}: {temps[i]} ℃  {(0.05+power[i]):.1f} W  ', end='' )
    print()


def main():
    'Main control loop, runs "forever"'
    load_config()
    announce_pid()
    signal.signal( signal.SIGUSR1, signal_handler_usr1 )
    signal.signal( signal.SIGTERM, signal_handler_term )
    # FIXME move this to configuration file
    os.environ['http_proxy'] = 'http://delphoenix.atnf.csiro.au:8888'
    os.environ['https_proxy'] = 'http://delphoenix.atnf.csiro.au:8888'

    fan = SupermicroFan()
    current_speed, fan_mode_str = fan.get_speed()
    print( 'Fan speed on startup:', fan_mode_str )
    pcie_slots = get_alveo_bdfs()
    while True:
        temps, power = get_alveo_sensor_readings( pcie_slots )
        log( pcie_slots, temps, power )
        max_temp = max( temps )
        fan.adjust_speed( max_temp )
        time.sleep( TEMPERATURE_CHECK_PERIOD_SEC )


main()
