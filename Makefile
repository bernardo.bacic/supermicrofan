#
# Main targets:
#   deb   - create a Debian *.deb package
#   clean - remove 
#
VERSION ?= 0.1.0
PROJECT_DIR := fan-control_$(VERSION)
BIN_DIR     := opt/skao/bin
SERVICE_DIR := usr/lib/systemd/system
UTIL_DIR    := usr/local/bin
BUILD_DIR   := build


# default target
help:
	@echo "deb   ... create Debian *.deb package"
	@echo "clean ... remove nonessential files"; echo

# build debian package (*.deb)
deb:
	@rm -rf $(PROJECT_DIR)
	@for i in $(PROJECT_DIR)/DEBIAN         \
	          $(PROJECT_DIR)/$(BIN_DIR)     \
	          $(PROJECT_DIR)/$(SERVICE_DIR) \
	          $(PROJECT_DIR)/$(UTIL_DIR)    \
	          $(BUILD_DIR);                 \
	do                                      \
	    mkdir -p $$i;                       \
	done
	@cp src/fan_control.py           $(PROJECT_DIR)/$(BIN_DIR)
	@cp resources/fan.conf           $(PROJECT_DIR)/$(BIN_DIR)
	@cp resources/packaging.control  $(PROJECT_DIR)/DEBIAN/control
	@echo Building VERSION: ${VERSION},   DEBIAN/control $$(grep Version resources/packaging.control)
	@cp resources/supermicro-fan-control.service  $(PROJECT_DIR)/$(SERVICE_DIR)/
	@cp resources/ipmicfg                         $(PROJECT_DIR)/$(UTIL_DIR)
	@sudo chown -R root. $(PROJECT_DIR)
	@[ "${VERSION}" = $$(awk '/^Version/ { print $$2 }' resources/packaging.control) ] || \
       (echo "ERROR: version mismatch"; exit 1)
	@dpkg --build $(PROJECT_DIR) $(BUILD_DIR)

clean: 
	@sudo rm -irf $$(find -type d -regex "\./fan-control_[0-9]+\.[0-9]+[.0-9]*")
	@rm -rf build/
