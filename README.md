# INTRO
Control Supermicro server (SYS-420GP-TNR) fan speed depending on Xilinx U55C Alveo card FPGA temperature speed.

## INSTALLATION
 `$ sudo dpkg -i fan_con
   it uses IPMI interface to monitor and control the fan

## RUNNING AS A DAEMON
Starting/stopping the daemon:

```
 $ sudo systemctl start supermicro-fan-control
 $ sudo systemctl stop  supermicro-fan-control
```

When service configuration file (/usr/lib/systemd/system/supermicro-fan-control.service) changes, run:

```
 $ sudo systemctl daemon-reload
 $ sudo systemctl restart  supermicro-fan-control
```

To monitor the fan/Alveo card related log entries:
  `$ journalctl -u supermicro-fan-control -f`

## FAN CONTROL
From the command line, one can use `ipmicfg` utility to control the fan speed:

```
 $ sudo ipmicfg -fan 2
 Current Fan Speed Mode is [ Optimal Mode ]

 Supported Fan modes:
 1:Full
 2:Optimal
 4:Heavy IO
```

Fan speeds for different modes are as follows:
 - ~3920 RPM for 'Optimal' mode
 - ~5180 RPM for 'Heavy IO' mode
 - ~11000 RPM for 'Full' mode

## RUNNING IN A CONTAINER (not yet)
 Readnig FPGA temperature works fine in  container but didn't manage to get the fan
 control working - relies on IPMI interface and /dev/ipmi0 device which is resisting
 mapping.

# NEXT
 - (official) directory structure
    see https://developer.skao.int/en/latest/tools/codeguides/python-codeguide.html
    also https://developer.skao.int/en/latest/projects/create-new-project.html
 - Controller class
 - block diagram
 - configuration file: temperature thresholds
 - is there an IPMI API we can use instead of 'ipmicfg' utility
   https://www.supermicro.com/en/solutions/management-software/ipmi-utilities
   https://github.com/ipmitool/ipmitool
   `-> sudo ipmitool chassis status ... gives fan fault status
 - notifications/alarms
 - Tango?
 - [[https://en.wikipedia.org/wiki/Redfish_(specification)][redfish]]
 - REVISIT
   * docker Dockerfile/image - NAH, couldn't get IPMI working in a container
   * ipmicfg won't run in a container: /dev/ipmi0 mapping issue
   * deployment in the kubernetes

# ADMIN
 - REPO https://gitlab.com/bernardo.bacic/supermicrofan
 - run docker container:
   `podman run --rm -it --device=/dev/xclmgmt21248:/dev/xclmgmt21248 --device=/dev/dri/renderD128:/dev/dri/renderD128 sm_fan_control:3`

 OLD:
  `$ docker run -it --volume="$(pwd):/xrt:ro" --rm --device=/dev/xclmgmt21248:/dev/xclmgmt21248 --device=/dev/dri/renderD128:/dev/dri/renderD128 -e http_proxy -e https_proxy --security-opt seccomp=unconfined <image_hash>`
